# TeamCity Build Agent template for BastilleBSD

Template for [BastilleBSD](https://bastillebsd.org/) to run a
[TeamCity](https://www.jetbrains.com/teamcity/) Build Agent inside of a
[FreeBSD](https://www.freebsd.org/) jail.

By default the hard coded version of TeamCity (see
[Bastillefile](Bastillefile)) will be installed using default settings.

You have to specify an external directory to be mounted as the agents
primary data directory and the URL to the TeamCity server. The specification
of a build agent name and server authorization token are optional.

Please note that you should run only one agent per jail and may have to
adjust the `ADDITIONAL_PKGS` parameter to install needed dependencies for
your builds.

**If you need another JDK for the build agent, please specify the path via
the `AGENT_JDK` parameter.**

## License

This program is distributed under 3-Clause BSD license. See the file
[LICENSE](LICENSE) for details.

## Bootstrap

So far bastille only supports downloading from GitHub or GitLab, so you have
to fetch the template manually:

```
# mkdir <your-bastille-template-dir>/wegtam
# git -C <your-bastille-template-dir>/wegtam clone https://codeberg.org/wegtam/bastille-teamcity-build-agent.git
```

## Usage

### 1. Install the default version into a jail

```
# bastille template TARGET wegtam/bastille-teamcity-build-agent \
  --arg EXT_DIR=/srv/agent --arg SERVER_URL=http://localhost:8080
```

### 2. Install with custom settings

```
# bastille template TARGET wegtam/bastille-teamcity-build-agent \
  --arg EXT_DIR=/srv/agent --arg SERVER_URL=http://localhost:8080 \
  --arg AGENT_NAME=LazyDog --arg AGENT_AUTH=XXX
```

For more options take a look at the [Bastillefile](Bastillefile).

